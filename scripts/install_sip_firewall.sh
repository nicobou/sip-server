#!/bin/bash
# Copy iptables SIP firewall service script and rules file
# Add systemd service to automatically start firewall at boot

mkdir -p /etc/iptables
cp config/iptables-sip-firewall.rules /etc/iptables/iptables-sip-firewall.rules

cp config/iptables-sip-firewall.sh /sbin/iptables-sip-firewall.sh
chown root:root /sbin/iptables-sip-firewall.sh
chmod 750 /sbin/iptables-sip-firewall.sh

cp config/iptables-sip-firewall.service /etc/systemd/system/iptables-sip-firewall.service
systemctl daemon-reload
systemctl enable iptables-sip-firewall
systemctl restart iptables-sip-firewall