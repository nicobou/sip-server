#!/bin/bash
# Copy config samples and replace localhost in config samples with
# given domain name
# If passed as 2nd argument, replaces default admin password in ldap config file
# If passed as 3rd argument, replaces default SIP password in ldap init.ldif seed file

if [ -z "$1" ]; then
  cat <<- EOF
Usage: $0 domain_name [ldap_admin_password] [sip_password]

	domain_name	example: example.com
	ldap_admin_password (optional base64-alphabet 8 chars. min.)	
		example: cmFuZG9tLXN0cmluZw==
	sip_password (optional base64-alphabet 8 chars. min.)	
		example: cmFuZG9tLXN0cmluZw==
EOF
  exit 1
fi

DOMAIN_NAME="$1"
LDAP_ADMIN_PASSWORD="$2"
SIP_PASSWORD="$3"
LDAP_DN=$( echo 'dc='$DOMAIN_NAME | sed 's/\./,dc=/g')
CONFIG_PATH=config
SAMPLE_PATH=$CONFIG_PATH/samples

sed 's/@localhost/@'$DOMAIN_NAME'/' $SAMPLE_PATH/init.ldif | \
	sed 's/dc=localhost/'$LDAP_DN'/' > $CONFIG_PATH/init.ldif
sed 's/=localhost/='$DOMAIN_NAME'/' $SAMPLE_PATH/ldap.env > $CONFIG_PATH/ldap.env
sed 's/=pla.localhost/=pla.'$DOMAIN_NAME'/' $SAMPLE_PATH/phpldapadmin.env > $CONFIG_PATH/phpldapadmin.env
sed -i 's/=localhost/='$DOMAIN_NAME'/' $CONFIG_PATH/phpldapadmin.env
sed 's/=localhost/='$DOMAIN_NAME'/' $SAMPLE_PATH/coturn.env > $CONFIG_PATH/coturn.env
sed 's/=localhost/='$DOMAIN_NAME'/' $SAMPLE_PATH/sip.env > $CONFIG_PATH/sip.env
sed 's/=localhost/='$DOMAIN_NAME'/' $SAMPLE_PATH/reflector.env > $CONFIG_PATH/reflector.env

# Replace "admin" string in ldap.env by ldap_admin_password if passed as argument
if [ -n "$LDAP_ADMIN_PASSWORD" ]; then \
        sed -i 's/=admin/='$LDAP_ADMIN_PASSWORD'/' $CONFIG_PATH/ldap.env; \
        sed -i 's/=admin/='$LDAP_ADMIN_PASSWORD'/' $CONFIG_PATH/phpldapadmin.env; \
		sed -i 's/=admin/='$LDAP_ADMIN_PASSWORD'/' $CONFIG_PATH/coturn.env; \
		sed -i 's/=admin/='$LDAP_ADMIN_PASSWORD'/' $CONFIG_PATH/sip.env; \
fi;

# Replace "sip_password" string in init.ldif and reflector service env file if sip_password passed as argument
if [ -n "$SIP_PASSWORD" ]; then \
        sed -i 's/sipidentitypassword: sip_password/sipidentitypassword: '$SIP_PASSWORD'/' $CONFIG_PATH/init.ldif; \
		sed -i 's/=sip_password/='$SIP_PASSWORD'/' $CONFIG_PATH/reflector.env; \
fi;

sed 's/"localhost"/"'$DOMAIN_NAME'"/' $SAMPLE_PATH/iptables-sip-firewall.rules > $CONFIG_PATH/iptables-sip-firewall.rules

echo "Domain name set in SIP, LDAP env/database and SIP firewall service"
