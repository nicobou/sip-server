# SIP Server

A SIP/STUN/TURN server that is fully compatible with the [Scenic](https://gitlab.com/sat-mtl/tools/scenic/scenic) software stack. It is made of several Docker containers organized with **Docker Compose**. It includes a **[Freeswitch](https://freeswitch.com/)** SIP server alongside an **[OpenLDAP](https://www.openldap.org/)** server and **[phpLDAPadmin](http://phpldapadmin.sourceforge.net/wiki/index.php/Main_Page)** for user authentication and management.

This server is currently used inside the [Scènes Ouvertes network](https://sat.qc.ca/fr/scenes-ouvertes): as such, it has been battle-tested many times, and is suited for production use.

## Requirements

### Hardware

First and foremost, you will need access to a distant or local machine on which to deploy your SIP server. EC2 instances from AWS work well for this purpose (*t3.medium* instances have been tested with this repo successfully). Ensure that your machine meets or exceeds these requirements:

* Ubuntu 20.04 LTS
* CPU >= 2 cores
* RAM >= 4GB
* Root (`/`) partition >= 48GB

It is recommended that you copy your public SSH on the server for easy password-less access.

### Network

The following ports must be opened on your server and allowed by the eventual firewall (ports in *italic* are optional):

* 22 (SSH)
* 80 (HTTP)
* 443 (HTTPS)
* 3478 (STUN)
* *3479 (STUN Backup)*
* 5060 (SIP)
* *9060 (SIP Backup)*

Ports **3478**, **3479**, **5060** and **9060** must be openend for **TCP** and **UDP**. Only **TCP** is necessary for ports **22**, **80** and **443**.

In addition, make sure that your machine is accessible with **your own public domain name**. It is also a good idea to allow **ICMP** for your server, if you want to be able to ping your server.

### Software

Make sure that Python 3 is installed on your machine.

## Installation

### Setup Docker Repo

```shell
sudo apt update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88  # (This makes sure that the repo key is properly added to `apt`)
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```

### Install Docker CE

```shell
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io
sudo docker run hello-world  # (This allows you to confirm that Docker CE was properly installed)
```

### Install Docker Compose

```shell
sudo curl -L "https://github.com/docker/compose/releases/download/v2.1.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/libexec/docker/cli-plugins/docker-compose
sudo chmod +x /usr/libexec/docker/cli-plugins/docker-compose
```

### Create New User

```shell
sudo adduser sip-server  # ('sip-server' can be changed at will)
sudo usermod -aG docker sip-server  # Add user to 'docker' group
```

### Install `Sip-Server` Repo

```shell
su sip-server
cd ~
mkdir docker && cd docker
git clone https://gitlab.com/sat-mtl/tools/scenic/sip-server.git
cd sip-server
```

### Configure and launch services

```shell
scripts/init_domain.sh your.domain sip-password # (replace 'your.domain' with the actual domain name for your SIP server)
sudo scripts/install_sip_firewall.sh
docker compose up -d
scripts/restore_ldif.sh config/init.ldif
scripts/update_coturn_users.sh
docker restart sip-server-reflector-1 # restart reflector registration after importing users
```

Optionally, in `docker-compose.yml`, you may set a different location for data volumes by replacing all `./volumes` with `/srv/sip-server`, or any other directory. Make sure to apply these changes before running `docker compose up -d`.


## Usage

Your SIP server should now be operational. Using Scenic, you can connect to it using a valid *uid* and *SIPIdentityPassword*, and call other connected SIP contacts.

To ensure optimal performance, make sure that the **SIP ALG**, **SIP HELPER**, **SIP PASSTHROUGH**, **SIP VISCOUS ACTIVITY**, **SIP FRIENDLY-SCANNER** and all other similar options are **disabled** in your firewall's configuration.

### LDAP User Management

It is highly recommended that you initialize the LDAP database with the included structure (defined in *config/init.ldif*). This will replicate a very basic database on your server, including three test users (`sat`, `sat2` and `sat3`). Be aware that *SIPIdentityPasswords* are stored in clear text in the LDIF file.

Users with the *inetOrgPerson* object class will be allowed to log in phpLDAPadmin using their *uid* and *userPassword*. Those who are in the **`cn=admin`** group will have administrative privileges on the database.

Users with the *SIPIdentity* class object will be allowed to log in the SIP server using their *uid* and *SIPIdentityPassword*.

To add, remove or update users on the server, you can use either **phpLDAPadmin** or your server's terminal.

#### phpLDAPadmin

To access the LDAP database through *phpLDAPadmin*, simply access the URL defined by `VIRTUAL_HOST` in `config/phpmyadmin.env` (`http://pla.your.domain`) in a browser and log in using your credentials. The database administrator is `cn=admin,dc=your,dc=domain` and the password is the one defined by `LDAP_PASSWORD` in `config/ldap.env`.

##### Troubleshooting

In case of a `403` error when connecting to phpLDAPadmin, run the following command and refresh your page: `docker restart sip-server-nginx-1`

If the `403` error persists, make sure that your NGINX config is correctly set. To check your NGINX config:

```shell
docker exec -it sip-server-nginx-1 /bin/bash  # Enter the NGINX container using an interactive shell
cd /etc/nginx/conf.d
cat default.conf
```

The configuration file should be identical to `config/samples/example_nginx_config.conf`. If the files differ, you will need to manually create a new config.

Follow these steps to create a new working config for NGINX:

1. Get the IP address of the `sip-server-phpldapadmin_1` container.

    1. Exit the `sip-server-nginx-1` container with `exit`.
    1. Type `docker ps` in the Terminal.
    1. Make note of the `CONTAINER ID` for `sip-server-phpldapadmin-1`.
    1. Get the container's IP address:

        ```shell
        # Replace <container_id> with the actual ID of sip-server-phpldapadmin-1
        docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <container_id>
        ```

1. Go back inside the `sip-server-nginx-1` container and install `nano`:

    ```shell
    docker exec -it sip-server-nginx-1 /bin/bash
    apt update
    apt install nano
    ```

1. Open `/etc/nginx/conf.d/default.conf` with `nano` and delete everything.

1. Paste in the contents of `example_nginx_config.conf`.

1. Replace all occurences of `pla.your.domain` with your actual domain name (it should also be identical to the value of `VIRTUAL_HOST` defined in `config/phpldapadmin.env`.)

1. Find the following line in `default.conf` and change it so that the `<ip_address>` field corresponds to the IP address obtained in step 1.

    ```conf
    server <ip_address>:443;
    ```

1. Save your new `default.conf` file and exit `nano`.

1. Test and reload the NGINX service:

    ```shell
    service nginx configtest  # Optional; tests your NGINX config and makes sure it is valid
    service nginx reload
    ```

1. Exit the container and try accessing phpLDAPadmin again from your browser.

#### Terminal

To manage users using the Terminal, use `ldapmodify` or `ldapvi` inside the *openldap* container.

You can easily list your openLDAP database on your server terminal by typing the following command:

```shell
docker exec sip-server-openldap-1 ldapsearch -x -b "dc=your,dc=domain" -D "cn=admin,dc=your,dc=domain" -w LDAP_PASSWORD
```

## Helper Scripts

Four helper scripts are included in `scripts/`:

* **`init_domain.sh`** : Sets the domain name in the `.env` files, `init.ldif` database and `iptables-sip-firewall.rules` iptables rules file, and copies them in the `config/` directory. Sets SIP password used by back-end services such as reflector.
* **`install_sip_firewall.sh`** : Copies iptables-sip-firewall files and setup systemd iptables-sip-firewall service to start at boot.
* **`restore_ldif.sh`** : Restores the default LDAP database.
* **`update_coturn_users.sh`** : Forces the import of all LDAP users in the STUN/TURN server database.
* **`make_authors_from_git.sh`** : Updates the `AUTHORS.md` file using the repo's git history.

The **`coturn`** server embeds a cronjob which syncs its database with the LDAP database every 5 minutes. However, it is still recommended to manually run `update_coturn_users.sh` each time a user is added to or deleted from the LDAP database

## SIP debugging using sngrep

To visualize SIP requests going in and out of sip-server, you can use sngrep tool.

You can easily install, start sngrep and filter on your clients public IPs by typing the following commands:

```shell
sudo apt install sngrep
sudo sngrep host 123.234.123.234 or host 234.123.234.123
```

## Interactive FreeSWITCH debugging using fs_cli

You can easily connect to FreeSWITCH service using fs_cli by typing the following commands:

```shell
docker exec -it sip-server-freeswitch-1 /usr/local/freeswitch/bin/fs_cli
```

To quit fs_cli command prompt, simply type `/quit` command.

You can list details about current registrations by typing the following command at fs_cli prompt:

```shell
sofia status profile default reg
```

You can obtain interessing informations such as state of NAT processing mode, media handling options, number of active registrations, number of succesful and failed calls by typing the following command at fs_cli prompt:

```shell
sofia status profile default
```

You can list active calls or channels by typing the following command at fs_cli prompt:

```shell
show calls
show channels
```

## Interactive coturn debugging using telnet cli

You can easily connect to coturn service cli using telnet by typing the following commands:

```shell
docker exec -it sip-server-coturn-1 /usr/bin/telnet localhost 5766
```

Enter password `coturncli`.

To quit telnet cli command prompt, simply type `quit` command.

You can list details about current registrations by typing the following command at telnet prompt:

```shell
?
```

You can list active sessions by typing the following command at telnet prompt:

```shell
ps
```

You can list active users by typing the following command at telnet prompt:

```shell
pu
```

You can list active allocations by typing the following command at telnet prompt:

```shell
pc
```

## CI runner configuration

This project CI pipeline requires a dedicated VPS server with an public IPv4 address.

You need to setup DNS entries ci1.sip.scenic.sat.qc.ca and pla.ci1.sip.scenic.sat.qc.ca to point to this IP before running pipeline.

Fetch registration token from GitLab project's repository and register the specific runner :

* In Settings > CI/CD page, expand Runners section.
* Copy Specific runners registration token.
* Paste token in the last command to register new runner.
* Add "sip-server" tag at the interactive question step "Enter tags for the runner".
* Choose "shell" executor type.

```shell
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
sudo gitlab-runner register --url https://gitlab.com/ --registration-token <registration-token-from-gitlab>
```

Allow gitlab-runner user to run sudo commands without password :

```shell
sudo visudo
# add following line to sudoers file.
gitlab-runner ALL=(ALL) NOPASSWD:ALL
```

Add project CI variable for LDAP admin password:

* In Settings > CI/CD page, expand Variables section.
* Select the Add Variable button and fill in the details:
  * Key: LDAP_ADMIN_PASSWORD
  * Value: <base64-random-passphrase>
  * Type: Variable
  * Environment scope: All
  * Protect variable: Unchecked (was checked)
  * Mask variable: Checked

Add project CI variable for SIP users password:

* In Settings > CI/CD page, expand Variables section.
* Select the Add Variable button and fill in the details:
  * Key: SIP_PASSWORD
  * Value: <base64-random-passphrase>
  * Type: Variable
  * Environment scope: All
  * Protect variable: Unchecked (was checked)
  * Mask variable: Checked

Variables must be 8 characters or longer, consisting only of:

* Characters from the Base64 alphabet (RFC4648).
* The @ and : characters (In GitLab 12.2 and later).
* The . character (In GitLab 12.10 and later).
* The ~ character (In GitLab 13.12 and later). 

## Building Docker images

See [Building Docker images](docs/building-docker-images.md).

## Authors

See [here](AUTHORS.md).

## License

This project is licensed under the GNU General Public License version 3 - see the [LICENSE](LICENSE.md) file for details.

## Acknowledgments

This project is a fork of the original [scenic-server](https://gitlab.com/sat-metalab/scenic-server) project created by the [Metalab](https://sat.qc.ca/fr/recherche/metalab) team for the [Société des Arts Technologiques (SAT)](http://sat.qc.ca/).
