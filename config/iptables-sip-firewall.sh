#!/bin/bash
# Manages iptables SIP firewall rules

# systemd service configuration ref. 
#   https://sleeplessbeastie.eu/2018/10/01/how-to-make-iptables-configuration-persistent-using-systemd/
# iptables rules based on Jan Taczanowski's article
#   https://taczanowski.net/securing-asterisk-sip-pbx-by-simple-iptables-rule-checking-if-the-domain-is-correct/
# Docker compatible iptables firewall example
#   https://unrouted.io/2017/08/15/docker-firewall/

# Drops any SIP packets not containing server domain name. Most SIP scanners tries to exploit via IP. This reduces 99% of background noise.
# You need to replace 'localhost' by the 'sip.domain.name.tld' used by your SIP application.

# Limit PATH
PATH="/sbin:/usr/sbin:/bin:/usr/bin"

# iptables configuration
sip_firewall_start() {
  iptables-restore --noflush < /etc/iptables/iptables-sip-firewall.rules
}

# clear iptables configuration
sip_firewall_stop() {
  iptables -F SIP-FILTERS
}

# execute action
case "$1" in
  start|restart)
    echo "Starting SIP firewall"
    sip_firewall_stop
    sip_firewall_start
    ;;
  stop)
    echo "Stopping SIP firewall"
    sip_firewall_stop
    ;;
esac