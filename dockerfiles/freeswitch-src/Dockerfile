FROM debian:buster-slim
MAINTAINER Scenic team <scenic-dev@sat.qc.ca>

# Signalwire deb repo http authentication token
# https://freeswitch.org/confluence/display/FREESWITCH/HOWTO+Create+a+SignalWire+Personal+Access+Token
ARG SIGNALWIRE_TOKEN=YOURSIGNALWIRETOKEN

ENV DEBIAN_FRONTEND noninteractive

# Add build dependencies source package repo
RUN apt-get update -qy && \
    apt-get install -qy gnupg2 wget lsb-release curl cron lua-cjson && \
# Add SignalWire deb repo key
    wget --http-user=signalwire --http-password=$SIGNALWIRE_TOKEN -O /usr/share/keyrings/signalwire-freeswitch-repo.gpg https://freeswitch.signalwire.com/repo/deb/debian-release/signalwire-freeswitch-repo.gpg && \
# Temporarly add SignalWire http auth token
    echo "machine freeswitch.signalwire.com login signalwire password $SIGNALWIRE_TOKEN" > /etc/apt/auth.conf && \
    chmod 600 /etc/apt/auth.conf && \
    echo "deb [signed-by=/usr/share/keyrings/signalwire-freeswitch-repo.gpg] https://freeswitch.signalwire.com/repo/deb/debian-release/ `lsb_release -sc` main" > /etc/apt/sources.list.d/freeswitch.list && \
    echo "deb-src [signed-by=/usr/share/keyrings/signalwire-freeswitch-repo.gpg] https://freeswitch.signalwire.com/repo/deb/debian-release/ `lsb_release -sc` main" >> /etc/apt/sources.list.d/freeswitch.list && \
# Fetch build dependencies
    apt-get update -qy && \
    apt-get build-dep -qy freeswitch && \
# Remove SignalWire http auth token
    rm /etc/apt/auth.conf && \
    apt-get autoremove -qy --purge && \
    apt-get clean -qy && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Fetch sources and remove default module configuration file
RUN cd /usr/src/ && \
    git clone https://github.com/signalwire/freeswitch.git -bv1.10.6 freeswitch && \
    cd /usr/src/freeswitch && \
    ./bootstrap.sh -j && \
    mv modules.conf modules.conf.orig

# Select custom list of modules to compile
COPY modules.conf /usr/src/freeswitch/modules.conf

# Compile and install
RUN cd /usr/src/freeswitch && \
    ./configure && \
    make && \
    make install

# Rename default configuration
RUN mv /usr/local/freeswitch/conf /usr/local/freeswitch/conf.orig

# Copy custom configuration
COPY freeswitch.xml /usr/local/freeswitch/conf/freeswitch.xml
COPY vars.xml /usr/local/freeswitch/conf/vars.xml
COPY ldap.conf /etc/ldap/ldap.conf

# Copy custom lua script
COPY flush-user-calls.lua /usr/local/freeswitch/scripts/flush-user-calls.lua

# Create user and group
RUN groupadd -r freeswitch --gid=10000 && useradd -r -g freeswitch --uid=10000 freeswitch

# Limits Configuration
COPY freeswitch.limits.conf /etc/security/limits.d/

# Copy log file pruning script
COPY freeswitch-maintenance.sh /usr/local/sbin
RUN chmod +x /usr/local/sbin/freeswitch-maintenance.sh

COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

EXPOSE 5060/tcp
EXPOSE 5060/udp
EXPOSE 9060/tcp
EXPOSE 9060/udp

ENTRYPOINT [ "/docker-entrypoint.sh" ]
CMD [ "freeswitch" ]