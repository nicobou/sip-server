#!/bin/bash

PROCNAME='turnserver'
DAEMON='/usr/bin/turnserver'
DAEMON_ARGS=( -c '/etc/coturn/turnserver.conf' -v )
USER='turnserver'

# Gets the external IP of the current container
#
# $SKIP_AUTO_IP - Skips this step
# $USE_IPV4 - Uses IPv4 instead of IPv6
#
# Returns the external IP 
function getExternalIp() {
  local external_ip

  if [ -z "${SKIP_AUTO_IP}" -a -z "${EXTERNAL_IP}" ]; then
    if [ -n "${USE_IPV4}" ]; then
      external_ip=`curl -4 icanhazip.com 2> /dev/null`
    else
      external_ip=`curl icanhazip.com 2> /dev/null`
    fi
  fi

  echo "$external_ip"
}

# Applies TURN server configuration
#
# Environment variables:
# $SIP_DOMAIN - SIP domain used
applyTurnServerConfig() {
  EXTERNAL_IP="$(getExternalIp)"
  if [ -n "${EXTERNAL_IP}" ]; then
    sed -i "s|^external-ip=.*|external-ip=${EXTERNAL_IP}|;" /etc/coturn/turnserver.conf
  fi

  if [ -n "${SIP_DOMAIN}" ]; then
    sed -i "s|^realm=.*|realm=${SIP_DOMAIN}|;" /etc/coturn/turnserver.conf
  fi
}

# Configures coturn environment variables and configuration
#
# Environment variables:
# $LDAP_ENV_LDAP_DOMAIN - 
# $LDAP_ENV_LDAP_PASSWORD
#
# Sets LDAP variables used by ldap2turn.sh script in /etc/coturn/envars
# Sets external-ip and realm in /etc/coturn/turnserver.conf
function configureCoturn() {
  [ ! -n "${LDAP_BASE_DN}"       ] && LDAP_BASE_DN=`echo "${LDAP_ENV_LDAP_DOMAIN}" | sed 's|^|dc=|;s|\.|,dc=|g;'`
  [ ! -n "${LDAP_BIND_DN}"       ] && LDAP_BIND_DN="cn=admin,${LDAP_BASE_DN}"
  [ ! -n "${LDAP_BIND_PASSWORD}" ] && LDAP_BIND_PASSWORD="${LDAP_ENV_LDAP_PASSWORD}"
  [ ! -n "${SIP_DOMAIN}"         ] && SIP_DOMAIN="${LDAP_ENV_LDAP_DOMAIN}"

  if [ ! -f /etc/coturn/envvars ]; then
    cat > /etc/coturn/envvars <<- EOF
	LDAP_BASE_DN="${LDAP_BASE_DN}"
	LDAP_BIND_DN="${LDAP_BIND_DN}"
	LDAP_BIND_PASSWORD="${LDAP_BIND_PASSWORD}"
	SIP_DOMAIN="${SIP_DOMAIN}"
EOF
  fi
}

# Setup cronjobs
# - Prunes logs every day at 6am
# - Syncs ldap users to coturn local db every 5 minutes
function configureCron() {
  service cron start
  echo '0 6 * * * /usr/local/sbin/coturn-maintenance.sh 
*/5 * * * * /usr/sbin/ldap2turn.sh' | crontab
}


# When called with executable name, replace with full path to daemon and append arguments.
if [ "${1}" = "${PROCNAME}" ]; then
  shift
  set -- "${DAEMON}" "${DAEMON_ARGS[@]}"
fi

if [ "$1" = "${DAEMON}" ]; then
  configureCoturn
  applyTurnServerConfig

  # Start as user
  if [ `id -u` = '0' ]; then
    set -- gosu "${USER}" "$@"
  fi

  chown turnserver:turnserver /var/log/coturn

  configureCron
fi

exec "$@"