# Building Docker images

## Build and push FreeSWITCH image

FreeSWITCH package repository now requires authentication.

SignalWire Personal Access Tokens (PAT)s are required to access FreeSWITCH install packages.

Follow this guide to create your token. [How to create a SignalWire Personal Access Token](https://freeswitch.org/confluence/display/FREESWITCH/HOWTO+Create+a+SignalWire+Personal+Access+Token).

Replace "pat_abc123" by your personal token. It gets passed as build argument, used to install repo during build, but will not be published in final image.

```bash
docker build \
      -t registry.gitlab.com/sat-mtl/tools/scenic/sip-server/freeswitch-src:2.1.3 \
      dockerfiles/freeswitch-src \
     --build-arg "SIGNALWIRE_TOKEN=pat_abc123"
docker login registry.gitlab.com
docker push registry.gitlab.com/sat-mtl/tools/scenic/sip-server/freeswitch-src:2.1.3
```

## Build and push coturn image

```bash
docker build \
      -t registry.gitlab.com/sat-mtl/tools/scenic/sip-server/coturn:2.1.2 \
      dockerfiles/coturn
docker login registry.gitlab.com
docker push registry.gitlab.com/sat-mtl/tools/scenic/sip-server/coturn:2.1.2
```

## Build and push Switcher image for version 2.1.38

```bash
docker build \
      -t registry.gitlab.com/sat-mtl/tools/scenic/sip-server/switcher:2.1.38 \
      --build-arg SWITCHER_TAG=2.1.38 \
      dockerfiles/switcher
docker login registry.gitlab.com
docker push registry.gitlab.com/sat-mtl/tools/scenic/sip-server/switcher:2.1.38
```

## Build and push Switcher image for version 3.1.14

```bash
docker build \
      -t registry.gitlab.com/sat-mtl/tools/scenic/sip-server/switcher:3.1.14 \
      --build-arg SWITCHER_TAG=3.1.14 \
      dockerfiles/switcher
docker login registry.gitlab.com
docker push registry.gitlab.com/sat-mtl/tools/scenic/sip-server/switcher:3.1.14
```