#!/bin/bash
# Simple 2-party 1-way SIP test
# - Start two Switcher instances named callee and caller,
# - register callee and caller to SIP server,
# - caller call callee,
# - caller hang-up,
# - caller call callee again,
# - callee hang-up, (BUG caller doesn't receive BYE and stays in calling state)
# - unregister callee and caller,
# - send interrupt signal to both Switcher processes.
# Each step result is tested by querying SIP quiddity status

if [ -z "$6" ]; then
  cat <<- EOF
Usage: $0 sip_domain sip_server_port callee_username callee_password caller_username caller_password

	sip_domain_name     example: sip.example.com
    sip_server_port     example: 5060 or 9060
    callee_username     example: sat
    callee_password     example: s3c3r3t
    caller_username     example: sat2
    caller_password     example: s3c3r3t
EOF
  exit 1
fi

# SIP users and server variables
SIP_SERVER="$1"
shift
SIP_SERVER_PORT="$1"
shift
CALLEE_USER="$1"
shift
CALLEE_PASSWORD="$1"
shift
CALLER_USER="$1"
shift
CALLER_PASSWORD="$1"
shift

# Switcher processes PIDs
CALLEE_PID=0
CALLER_PID=0

oneTimeSetUp() {
    # Start callee Switcher process and redirect outputs to log file
    echo "> Starting callee Switcher instance"
    switcher -d -n callee 2>&1 > switcher-callee.log &
    CALLEE_PID=$!
    echo "> callee switcher PID: $CALLEE_PID"

    # Start caller Switcher process and redirect outputs to log file
    echo "> Starting caller Switcher instance"
    switcher -d -p 15432 -n caller &> switcher-caller.log &
    CALLER_PID=$!
    echo "> caller switcher PID: $CALLER_PID"

    sleep 2

    echo "> Verifying callee switcher process is running"
    ps -o pid= -o comm= -p $CALLEE_PID
    assertTrue "Callee Switcher process not found." $?

    echo "> Verifying caller switcher process is running"
    ps -o pid= -o comm= -p $CALLER_PID
    assertTrue "Caller Switcher process not found." $?
}

testRegisterCallee() {
    # Register callee, set STUN/TURN, add buddy
    echo "> Creating callee sip quiddty"
    switcher-ctrl -C sip sip

    echo "> Registering $CALLEE_USER@$SIP_SERVER:$SIP_SERVER_PORT"
    switcher-ctrl -s sip port 5060
    switcher-ctrl -i sip register $CALLEE_USER@$SIP_SERVER:$SIP_SERVER_PORT $CALLEE_PASSWORD

    echo "> Setting STUN/TURN service"
    output_stun_registration=$(switcher-ctrl -i sip set_stun_turn $SIP_SERVER $SIP_SERVER $CALLEE_USER $CALLEE_PASSWORD)
    assertEquals "Callee switcher STUN registration is not returning active state" "true" $output_stun_registration

    echo "> Adding buddy $CALLER_USER@$SIP_SERVER"
    switcher-ctrl -s sip mode "authorized contacts"
    switcher-ctrl -i sip add_buddy $CALLER_USER@$SIP_SERVER

    echo "> Authorize $CALLER_USER@$SIP_SERVER"
    switcher-ctrl -i sip authorize $CALLER_USER@$SIP_SERVER true

    # Test if callee registration is active
    echo "> Verify callee registration state"
    output_sip_registration=$(switcher-ctrl -g sip sip-registration)
    assertEquals "Callee switcher registration is not returning active state" "true" $output_sip_registration
}

testRegisterCaller() {
    # Register caller, set STUN/TURN, add buddy
    echo "Creating caller sip quiddty"
    switcher-ctrl --server http://localhost:15432 -C sip sip
    
    echo "Registering $CALLER_USER@$SIP_SERVER:$SIP_SERVER_PORT"
    switcher-ctrl --server http://localhost:15432 -s sip port 5061
    switcher-ctrl --server http://localhost:15432 -i sip register $CALLER_USER@$SIP_SERVER:$SIP_SERVER_PORT $CALLER_PASSWORD
    
    echo "Setting STUN/TURN service"
    output_stun_registration=$(switcher-ctrl --server http://localhost:15432 -i sip set_stun_turn $SIP_SERVER $SIP_SERVER $CALLER_USER $CALLER_PASSWORD)
    assertEquals "Caller switcher STUN registration is not returning active state" "true" $output_stun_registration

    echo "Adding buddy $CALLEE_USER@$SIP_SERVER"
    switcher-ctrl --server http://localhost:15432 -i sip add_buddy $CALLEE_USER@$SIP_SERVER

    # Test if caller registration is active
    echo "> Verify caller registration state is active"
    output_sip_registration=$(switcher-ctrl --server http://localhost:15432 -g sip sip-registration)
    assertEquals "Caller switcher registration is not returning active state" "true" $output_sip_registration
}

testAddCallerAudioTestSource() {
    # Add test audio source
    echo "> Add caller test audio source"
    switcher-ctrl --server http://localhost:15432 -C audiotestsrc aud1
    switcher-ctrl --server http://localhost:15432 -s aud1 wave 0
    switcher-ctrl --server http://localhost:15432 -s aud1 started true

    # Attach audio source to callee contact
    echo "Attach audio source to callee contact"

    switcher-ctrl --server http://localhost:15432 -i sip attach_shmdata_to_contact $(switcher-ctrl --server http://localhost:15432 -p aud1 audio) $CALLEE_USER@$SIP_SERVER true

    # Test if caller audio source is active
    echo "> Verify caller audio source state is active"
    output_audio_started=$(switcher-ctrl --server http://localhost:15432 -g aud1 started)
    assertEquals "Caller switcher audio source is not returning active state" "true" $output_audio_started
}

testCallerCallsCallee() {
    # Start sending
    echo "Call $CALLEE_USER@$SIP_SERVER from $CALLER_USER@$SIP_SERVER and wait 2 seconds"
    switcher-ctrl --server http://localhost:15432 -i sip send $CALLEE_USER@$SIP_SERVER
    sleep 2

    # Test if caller is in call
    echo "> Verify caller is currently calling callee"
    output_caller_send_status=$(switcher-ctrl --server http://localhost:15432 -t sip buddies.0.send_status)
    assertEquals "Caller send_status is not \"calling\"" "\"calling\"" $output_caller_send_status

    # Test if callee is in call
    echo "> Verify callee is currently receiving caller"
    output_callee_recv_status=$(switcher-ctrl -t sip buddies.0.recv_status)
    assertEquals "Callee recv_status is not \"receiving\"" "\"receiving\"" $output_callee_recv_status
}

testCallerHangUpCallee() {
    # Hang-up call from caller side
    echo "> Hang-up call from caller side and wait 2 seconds"
    switcher-ctrl --server http://localhost:15432 -i sip hang-up $CALLEE_USER@$SIP_SERVER
    sleep 2

    # Test if caller is not in call
    echo "> Verify caller is disconnected from callee"
    output_caller_send_status=$(switcher-ctrl --server http://localhost:15432 -t sip buddies.0.send_status)
    assertEquals "Caller send_status is not \"disconnected\"" "\"disconnected\"" $output_caller_send_status

    # Test if callee is not in call
    echo "> Verify callee is disconnected from caller"
    output_callee_recv_status=$(switcher-ctrl -t sip buddies.0.recv_status)
    assertEquals "Callee recv_status is not \"disconnected \"" "\"disconnected\"" $output_callee_recv_status
}

testCallerCallsCallee2ndTime() {
    echo "> Caller callback callee"
    testCallerCallsCallee
}

testCalleeHangUpCaller() {
    # Hang-up call from callee side
    echo "> Hang-up call from callee side and wait 2 seconds"
    switcher-ctrl -i sip hang-up $CALLER_USER@$SIP_SERVER
    sleep 2

    # TODO: BYE does not get relayed to caller by server for the moment.
    # It appears that server relay BYE to wrong IP:PORT. This is probably caused by server-side NAT issue.
    # For now we skip testing if caller is still in call.
    echo "> Skipping verifying caller is disconnected from callee. TODO: caller not does get BYE relayed by server."
    output_caller_send_status=$(switcher-ctrl --server http://localhost:15432 -t sip buddies.0.send_status)
    echo "Caller send_status is still in $output_caller_send_status state."

    ## Test if caller is not in call
    #echo "> Verify caller is disconnected from callee"
    #output_caller_send_status=$(switcher-ctrl --server http://localhost:15432 -t sip buddies.0.send_status)
    #assertEquals "Caller send_status is not \"disconnected\"" "\"disconnected\"" $output_caller_send_status

    # Test if callee is not in call
    echo "> Verify callee is disconnected from caller"
    output_callee_recv_status=$(switcher-ctrl -t sip buddies.0.recv_status)
    assertEquals "Callee recv_status is not \"disconnected \"" "\"disconnected\"" $output_callee_recv_status
}

testUnregisterCallee() {
    # Unregister callee
    echo "Unregistering callee"
    switcher-ctrl -i sip unregister

    # Test if callee registration is inactive
    echo "> Verify callee registration state is inactive"
    output_sip_registration=$(switcher-ctrl -g sip sip-registration)
    assertEquals "Callee switcher registration is still returning active state" "false" $output_sip_registration
}

testUnregisterCaller() {
    # Unregister caller
    echo "Unregistering caller"
    switcher-ctrl --server http://localhost:15432 -i sip unregister

    # Test if caller registration is inactive
    echo "> Verify caller registration state is inactive"
    output_sip_registration=$(switcher-ctrl --server http://localhost:15432 -g sip sip-registration)
    assertEquals "Caller switcher registration is still returning active state" "false" $output_sip_registration

}

testTearDown() {
    # Send interrupt signal to callee Switcher instance
    echo "> Sending interrupt signal to callee switcher PID $CALLEE_PID"
    kill -INT $CALLEE_PID

    # Send interrupt signal to callee Switcher instance
    echo "> Sending interrupt signal to caller switcher PID $CALLER_PID"
    kill -INT $CALLER_PID

    echo "> Wait 10 seconds for switcher processes to terminate"
    sleep 10

    echo "> Verifying callee switcher process is not running"
    ps -o pid= -o comm= -p $CALLEE_PID
    assertFalse "Callee Switcher process found." $?

    echo "> Verifying caller switcher process is not running"
    ps -o pid= -o comm= -p $CALLER_PID
    assertFalse "Caller Switcher process found." $?
}

# Load and run shUnit2 test suite

. /usr/bin/shunit2