#!/bin/bash
# 1-way SIP call to reflector service test
# - Start one Switcher instances named caller,
# - register caller to SIP server,
# - caller call reflector,
# - caller hang-up,
# - unregister caller,
# - send interrupt signal to Switcher process.
# Each step result is tested by querying SIP quiddity status

if [ -z "$4" ]; then
  cat <<- EOF
Usage: $0 sip_domain sip_server_port caller_username caller_password

	sip_domain_name     example: sip.example.com
    sip_server_port     example: 5060 or 9060
    caller_username     example: sat2
    caller_password     example: s3c3r3t
EOF
  exit 1
fi

# SIP users and server variables
SIP_SERVER="$1"
shift
SIP_SERVER_PORT="$1"
shift
CALLER_USER="$1"
shift
CALLER_PASSWORD="$1"
shift

# Switcher process PID
CALLER_PID=0

oneTimeSetUp() {
    # Start caller Switcher process and redirect outputs to log file
    echo "> Starting caller Switcher instance"
    switcher -d -n caller 2>&1 > switcher-caller.log &
    CALLER_PID=$!
    echo "> caller switcher PID: $CALLER_PID"

    sleep 2

    echo "> Verifying caller switcher process is running"
    ps -o pid= -o comm= -p $CALLER_PID
    assertTrue "Caller Switcher process not found." $?
}

testRegisterCaller() {
    # Register caller, set STUN/TURN, add buddy
    echo "Creating caller sip quiddty"
    switcher-ctrl -C sip sip
    
    echo "Registering $CALLER_USER@$SIP_SERVER:$SIP_SERVER_PORT"
    switcher-ctrl -s sip port 5060
    switcher-ctrl -i sip register $CALLER_USER@$SIP_SERVER:$SIP_SERVER_PORT $CALLER_PASSWORD
    
    echo "Setting STUN/TURN service"
    output_stun_registration=$(switcher-ctrl -i sip set_stun_turn $SIP_SERVER $SIP_SERVER $CALLER_USER $CALLER_PASSWORD)
    assertEquals "Caller switcher STUN registration is not returning active state" "true" $output_stun_registration

    echo "Adding buddy reflector@$SIP_SERVER"
    switcher-ctrl -i sip add_buddy reflector@$SIP_SERVER

    # Test if caller registration is active
    echo "> Verify caller registration state is active"
    output_sip_registration=$(switcher-ctrl -g sip sip-registration)
    assertEquals "Caller switcher registration is not returning active state" "true" $output_sip_registration
}

testAddCallerAudioTestSource() {
    # Add test audio source
    echo "> Add caller test audio source"
    switcher-ctrl -C audiotestsrc aud1
    switcher-ctrl -s aud1 wave 0
    switcher-ctrl -s aud1 started true

    # Attach audio source to reflector contact
    echo "Attach audio source to reflector contact"
    switcher-ctrl -i sip attach_shmdata_to_contact $(switcher-ctrl -p aud1 audio) reflector@$SIP_SERVER true

    # Test if caller audio source is active
    echo "> Verify caller audio source state is active"
    output_audio_started=$(switcher-ctrl -g aud1 started)
    assertEquals "Caller switcher audio source is not returning active state" "true" $output_audio_started
}

testCallerCallsReflector() {
    # Start sending
    echo "Call reflector@$SIP_SERVER from $CALLER_USER@$SIP_SERVER and wait 10 seconds"
    switcher-ctrl -i sip send reflector@$SIP_SERVER
    sleep 10

    # Test if caller is in call
    echo "> Verify caller is currently calling reflector"
    output_caller_send_status=$(switcher-ctrl -t sip buddies.0.send_status)
    assertEquals "Caller send_status is not \"calling\"" "\"calling\"" $output_caller_send_status

    # Test if caller is receiving back by reflector
    echo "> Verify caller is currently receiving reflector"
    output_caller_recv_status=$(switcher-ctrl -t sip buddies.0.recv_status)
    assertEquals "Caller recv_status is not \"receiving\"" "\"receiving\"" $output_caller_recv_status
}

testCallerHangUpReflector() {
    # Hang-up call from caller side
    echo "> Hang-up outbound call to reflector from caller side and wait 2 seconds"
    switcher-ctrl -i sip hang-up reflector@$SIP_SERVER
    sleep 2

    # Test if caller is not in call
    echo "> Verify caller is disconnected from reflector"
    output_caller_send_status=$(switcher-ctrl -t sip buddies.0.send_status)
    assertEquals "Caller send_status is not \"disconnected\"" "\"disconnected\"" $output_caller_send_status

    # Test if caller is not receiving from reflector
    echo "> Verify caller is disconnected from reflector"
    output_caller_recv_status=$(switcher-ctrl -t sip buddies.0.recv_status)
    assertEquals "Caller recv_status is not \"disconnected \"" "\"disconnected\"" $output_caller_recv_status
}

testUnregisterCaller() {
    # Unregister caller
    echo "Unregistering caller"
    switcher-ctrl -i sip unregister

    # Test if caller registration is inactive
    echo "> Verify caller registration state is inactive"
    output_sip_registration=$(switcher-ctrl -g sip sip-registration)
    assertEquals "Caller switcher registration is still returning active state" "false" $output_sip_registration

}

testTearDown() {
    # Send interrupt signal to caller Switcher instance
    echo "> Sending interrupt signal to caller switcher PID $CALLER_PID"
    kill -INT $CALLER_PID

    echo "> Wait 10 seconds for switcher process to terminate"
    sleep 10

    echo "> Verifying caller switcher process is not running"
    ps -o pid= -o comm= -p $CALLER_PID
    assertFalse "Caller Switcher process found." $?
}

# Load and run shUnit2 test suite

. /usr/bin/shunit2