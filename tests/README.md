# Building and pushing Switcher docker images (optional)

## Install Docker

```bash
sudo -S apt-get remove docker docker-engine docker.io

sudo apt-get install \
  apt-transport-https \
  ca-certificates \
  curl \
  software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | \
  sudo apt-key add -

sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"

sudo apt update
sudo apt-get install docker-ce

# Finally, Add group docker to current user
sudo usermod -a -G docker $USER
```

## Clone sip-server repository

```bash
git clone https://gitlab.com/sat-mtl/tools/scenic/sip-server.git
cd sip-server
git checkout devel
```

## Build Switcher image for version 2.1.38

```bash
docker build \
      -t registry.gitlab.com/sat-mtl/tools/scenic/sip-server/switcher:2.1.38 \
      --build-arg SWITCHER_TAG=2.1.38 \
      dockerfiles/switcher
```

## Push Switcher 2.1.38 image to project repository

```bash
docker login registry.gitlab.com
docker push registry.gitlab.com/sat-mtl/tools/scenic/sip-server/switcher:2.1.38
```

## Build Switcher image for version 3.0.1

```bash
docker build \
      -t registry.gitlab.com/sat-mtl/tools/scenic/sip-server/switcher:3.0.1 \
      --build-arg SWITCHER_TAG=3.0.1 \
      dockerfiles/switcher
```

## Push Switcher 3.0.1 image to project repository

```bash
docker login registry.gitlab.com
docker push registry.gitlab.com/sat-mtl/tools/scenic/sip-server/switcher:3.0.1
```

# Run SIP tests on Switcher images

Make sure you are in sip-server project directory. If not, checkout repository and change directory.

```bash
git clone https://gitlab.com/sat-mtl/tools/scenic/sip-server.git
cd sip-server
git checkout devel
```

## Test on Switcher 2.1.38

Start Switcher 2.1.38 container and enter its shell. We bind mount sip-server project directory into /opt/sip-server inside container.

```bash
docker run \
      -it \
      --mount type=bind,source="$(pwd)",target=/opt/sip-server \
      --name switcher-2.1.38_1 \
      registry.gitlab.com/sat-mtl/tools/scenic/sip-server/switcher:2.1.38
```

Start SIP test inside of docker container.

```bash
apt update && apt install -y jq shunit2
cd /opt/sip-server/tests/
./switcher-ctrl_2Party1WayCall.sh dev.sip.scenic.sat.qc.ca qasip1 password qasip2 password
exit
```

Stop and remove container after test.

```bash
docker rm switcher-2.1.38_1
```

## Test on Switcher 3.0.1

Start Switcher 3.0.1 container and enter its shell. We bind mount sip-server project directory into /opt/sip-server inside container.

```bash
docker run \
      -it \
      --mount type=bind,source="$(pwd)",target=/opt/sip-server \
      --name switcher-3.0.1_1 \
      registry.gitlab.com/sat-mtl/tools/scenic/sip-server/switcher:3.0.1
```

Start SIP test inside of docker container.

```bash
apt update && apt install -y jq shunit2
cd /opt/sip-server/tests/
./switcher-ctrl_2Party1WayCall.sh dev.sip.scenic.sat.qc.ca qasip1 password qasip2 password
exit
```

Stop and remove container after test.

```bash
docker rm switcher-3.0.1_1
```